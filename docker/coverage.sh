#!/bin/bash

cd /app
export GRADLE_HOME=/app/gradle-7.5.1
SPRING_PROFILES_ACTIVE=production $GRADLE_HOME/bin/gradle test
$GRADLE_HOME/bin/gradle jacocoTestReport
